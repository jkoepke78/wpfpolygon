﻿using SPITest.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SPITest.Controller
{
    /// <summary>
    /// Klasse zur Erzeugung unterschiedlicher Polygone
    /// </summary>
    public class PolygonFactory
    {
        #region member variables
            private PolygonGeometryData _PolyGeoData = new PolygonGeometryData();
        #endregion


        #region constructors
            /// <summary>
            /// 
            /// </summary>
            public PolygonFactory()
            {
                this.PolygonRefactory = true;
            }
        #endregion


        #region member function
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public Polygon CreatePolygon()
            {
                if (_PolyGeoData.Points.Count > 2)
                {
                    Polygon newPolygon = new Polygon();

                    newPolygon.Stroke = System.Windows.Media.Brushes.Black;
                    newPolygon.StrokeThickness = 2;

                    newPolygon.HorizontalAlignment = HorizontalAlignment.Left;
                    newPolygon.VerticalAlignment = VerticalAlignment.Center;

                    newPolygon.Fill = SpecialPolygon.DefaultPolygonColor;

                    PointCollection points = new PointCollection();
                    foreach (Point loopPt in PolygonGeometrieData.Points)
                    {
                        points.Add(new Point { 
                            X = loopPt.X + PolygonGeometrieData.OffsetX, 
                            Y = loopPt.Y + PolygonGeometrieData.OffsetY });
                    }

                    if (PolygonRefactory)
                    {
                        newPolygon.Points = PolygonPointsRefactoring(points);
                    }
                    else
                    {
                        newPolygon.Points = points;
                    }

                    return newPolygon;
                }

                return null;
            }

            /// <summary>
            /// Zeichnen eines Polygon beginnen bei einen beliebigen Punkt wird aus den restlichen Punkten der naechstliegende
            /// ausgewaehlt...Polygon soll eine geschlossene Flaeche erhalten und nicht wie beispielsweise ein X aussehen
            /// </summary>
            /// <param name="prmCollPoints"></param>
            /// <returns></returns>
            /// TODO noch Testen TODO
            private PointCollection PolygonPointsRefactoring(PointCollection prmCollPoints)
            {
                PointCollection pointCollection = new PointCollection();
                pointCollection.Add(prmCollPoints[0]);
                prmCollPoints.Remove(prmCollPoints[0]);
                
                int maxIndex = prmCollPoints.Count;
                for (int index = 0; index < maxIndex; index++)
                {
                    double minDistance = 0.0d;
                    Point pointToSave = new Point();

                    foreach(Point point in prmCollPoints)
                    {
                        //Naechstliegenden Punkt bestimmen:
                        double distance = Math.Sqrt(
                        Math.Pow((pointCollection[pointCollection.Count - 1].X - point.X), 2) +
                        Math.Pow((pointCollection[pointCollection.Count - 1].Y - point.Y), 2)
                        );

                        if (minDistance == 0.0f || minDistance > distance)
                        {
                            minDistance = distance;
                            pointToSave = point;
                        }
                    }
                    pointCollection.Add(pointToSave);
                    prmCollPoints.Remove(pointToSave);
                }

                return pointCollection;
            }
        #endregion


        #region properties
            /// <summary>
            /// 
            /// </summary>
            public PolygonGeometryData PolygonGeometrieData
            {
                get { return this._PolyGeoData; }
            }

            /// <summary>
            /// 
            /// </summary>
            public bool PolygonRefactory {get; set;}
        #endregion


        #region events
        #endregion
    }
}
