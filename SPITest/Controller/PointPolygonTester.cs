﻿using SPITest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SPITest.Controller
{
    public class PointPolygonTester
    {
        private Point _MouseClickPoint = new Point();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prmMouseClickPoint"></param>
        /// <param name="prmCollPolygons"></param>
        public void StartTest(Point prmMouseClickPoint, UIElementCollection prmCollPolygons)
        {
            this._MouseClickPoint = prmMouseClickPoint;

            foreach (var polygon in prmCollPolygons)
            {
                try
                {
                    if (PointInPolygon((Polygon)polygon) >= 0)
                    {
                        ((Polygon)polygon).Fill = SpecialPolygon.SelectedPolygonColor;
                        continue;
                    }

                    //Polygone bekommt Defaultfarbe verpasst...(wichtig für Polygone die zuvor selektiert waren und jetzt nicht mehr)
                    ((Polygon)polygon).Fill = SpecialPolygon.DefaultPolygonColor;
                }
                catch (Exception) { }
            }
        }
        
        /// <summary>
        /// Bei der Strahl-Methode wird von dem zu testenden Punkt (Klickpunkt) ein Strahl in eine beliebige Richtung versendet. 
        /// Dabei wird gezaehlt, wie oft der Strahl die Kanten des Polygons schneidet
        /// </summary>
        /// <param name="prmPolygon"></param>
        /// <returns></returns>
        private int PointInPolygon(Polygon prmPolygon)
        {
            int insidePolygon = -1;

            PointCollection points = prmPolygon.Points;
            points.Add(prmPolygon.Points[0]);

            for (int index = 0; index < points.Count-1; index++)
            {
                insidePolygon = insidePolygon * CrossProdTest(_MouseClickPoint, points[index], points[index + 1]);
            }

            return insidePolygon;
        }

        /// <summary>
        /// </summary>
        /// <param name="prmPointA"></param>
        /// <param name="prmPointB"></param>
        /// <returns>
        /// −1, wenn der Strahl von A nach rechts die Kante [BC] schneidet (außer im unteren Endpunkt);
        /// 0, wenn A auf [BC] liegt;
        /// sonst +1
        /// </returns>
        private int CrossProdTest(Point prmPointA, Point prmPointB, Point prmPointC)
        {
            //  Wenn y_A = y_B = y_C
            //    Wenn x_B ≤ x_A ≤ x_C oder x_C ≤ x_A ≤ x_B
            //      Ergebnis: 0
            //    sonst
            //      Ergebnis: +1
            if (prmPointA.Y == prmPointB.Y && prmPointA.Y == prmPointC.Y)
            {
                if ((prmPointB.X <= prmPointA.X && prmPointA.X <= prmPointC.X) || (prmPointC.X <= prmPointA.X && prmPointA.X <= prmPointB.X))
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            //  Wenn y_B > y_C
            //    Vertausche B und C
            if (prmPointB.Y > prmPointC.Y)
            {
                Point helpPoint = prmPointB;
                prmPointB = prmPointC;
                prmPointC = helpPoint;
            }
            //  Wenn y_A = y_B und x_A = x_B
            //    Ergebnis: 0
            if (prmPointA.Y == prmPointB.Y && prmPointA.X == prmPointB.X)
            {
                return 0;
            }
            //  Wenn y_A ≤ y_B oder y_A > y_C
            if (prmPointA.Y <= prmPointB.Y || prmPointA.Y > prmPointC.Y)
            {
                return 1;
            }
            //  Setze Delta = (x_B−x_A) * (y_C−y_A) − (y_B−y_A) * (x_C−x_A)
            double Delta = (prmPointB.X - prmPointA.X)*(prmPointC.Y-prmPointA.Y) - (prmPointB.Y - prmPointA.Y) * (prmPointC.X - prmPointA.X);
            //  Wenn Delta > 0
            //    Ergebnis: −1
            //  sonst wenn Delta < 0
            //    Ergebnis: +1
            //  sonst
            //    Ergebnis: 0
            if (Delta > 0)
            {
                return -1;
            }
            else if (Delta < 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
