﻿using SPITest.Controller;
using SPITest.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPITest
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region member variables
            private PolygonFactory _PolygonFactory;
            private Polygon _NewPolygon;
            private PointPolygonTester _PointPolygonTester = new PointPolygonTester();
        #endregion


        #region Initialization
            /// <summary>
            /// 
            /// </summary>
            public MainWindow()
            {
                InitializeComponent();
                CustomInitialization();
            }

            /// <summary>
            /// 
            /// </summary>
            private void CustomInitialization()
            {
                this.StPanelPolyPoints.Children.Add(CreateNewTextBoxLine());

                this._PolygonFactory = new PolygonFactory();
                this._NewPolygon = null;

                this._PolygonFactory.PolygonGeometrieData.Points.CollectionChanged += PolygonPoints_CollectionChanged;
                this._PolygonFactory.PolygonGeometrieData.PropertyChanged += PolygonGeometrieData_PropertyChanged;
            }
        #endregion


        #region member functions
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            private DockPanel CreateNewTextBoxLine()
            {
                Label newLabelX = new Label
                {
                    Content = "X-Value:",
                    Margin = new Thickness(5, 0, 5, 0)
                };
                TextBox newTxtBoxX = new TextBox
                {
                    Height = 23,
                    Width = 50,
                };
                newTxtBoxX.PreviewKeyUp += newTxtBox_PreviewKeyUp;

                Label newLabelY = new Label
                {
                    Content = "Y-Value:",
                    Margin = new Thickness(5, 0, 5, 0)
                };
                TextBox newTxtBoxY = new TextBox
                {
                    Height = 23,
                    Width = 50
                };
                newTxtBoxY.PreviewKeyUp += newTxtBox_PreviewKeyUp;

                DockPanel newDockPanel = new DockPanel();
                newDockPanel.Children.Add(newLabelX);
                newDockPanel.Children.Add(newTxtBoxX);
                newDockPanel.Children.Add(newLabelY);
                newDockPanel.Children.Add(newTxtBoxY);

                return newDockPanel;
            }

            /// <summary>
            /// Eckpunkte haben sich geaendert, Polygon wird neugezeichnet
            /// </summary>
            private void RepaintPolygon()
            {
                if (this._NewPolygon != null && this._NewPolygon.Points.Count >= 3)
                {
                    try
                    {
                        this.CanvasPaintground.Children.Remove(this._NewPolygon);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Es wurde versucht ein Polygon aus dem Zeichenbereich zu entfernen welches dort jedoch noch nicht existiert");
                    }
                }

                this._NewPolygon = this._PolygonFactory.CreatePolygon();
                if (this._NewPolygon != null && this._NewPolygon.Points.Count >= 3)
                {
                    this.CanvasPaintground.Children.Add(this._NewPolygon);
                }
            }
        #endregion

        
        #region Events
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void newTxtBox_PreviewKeyUp(object sender, KeyEventArgs e)
            {
                foreach(DockPanel loopDockPanel in this.StPanelPolyPoints.Children)
                {
                    int indexPolygonPoint = this.StPanelPolyPoints.Children.IndexOf(loopDockPanel);

                    if (loopDockPanel.Children.Count > 0)
                    {
                        //X- und Y-Wert auslesen und an PolygonFactory uebergeben
                        Point readPoint = new Point();

                        int xValue = 0;
                        if(Int32.TryParse(((TextBox)loopDockPanel.Children[1]).Text, out xValue)){
                            readPoint.X = xValue;
                        }
                        int yValue = 0;
                        if (Int32.TryParse(((TextBox)loopDockPanel.Children[3]).Text, out yValue))
                        {
                            readPoint.Y = yValue;
                        }

                        //PolygonPunkt schon vorhanden wird nur geaendert
                        if(this._PolygonFactory.PolygonGeometrieData.Points.Count > indexPolygonPoint){
                            this._PolygonFactory.PolygonGeometrieData.Points[indexPolygonPoint] = readPoint;
                        }
                        //PolygonPunkt noch nicht vorhanden wird neu erzeugt
                        else
                        {
                            this._PolygonFactory.PolygonGeometrieData.Points.Add(readPoint);
                        }
                    }

                    if(loopDockPanel.Children.Contains((TextBox)sender))
                    {
                        if (this.StPanelPolyPoints.Children.IndexOf(loopDockPanel) != this.StPanelPolyPoints.Children.Count-1){
                            return;
                        }
                    }
                }

                //Weitere Zeile mit Label und Textbox erzeugen, in der der User einen weiteren Punkt fuer das Polygon eintragen kann
                this.StPanelPolyPoints.Children.Add(CreateNewTextBoxLine());
            }

            /// <summary>
            /// Verschiebung des Polygons um einen X-Wert
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void TxtBxValueVertical_PreviewKeyUp(object sender, KeyEventArgs e)
            {
                int value = 0;
                if (Int32.TryParse(TxtBxValueVertical.Text, out value))
                {
                    this._PolygonFactory.PolygonGeometrieData.OffsetY = value;
                }
            }

            /// <summary>
            /// Verschiebung des Polygons um einen Y-Wert
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void TxtBxValueHorizontal_PreviewKeyUp(object sender, KeyEventArgs e)
            {
                int value = 0;
                if (Int32.TryParse(TxtBxValueHorizontal.Text, out value))
                {
                    this._PolygonFactory.PolygonGeometrieData.OffsetX = value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void PolygonPoints_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
            {
                this.RepaintPolygon();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void PolygonGeometrieData_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
            {
                this.RepaintPolygon();
            }

            /// <summary>
            /// Polygon wird im Zeichenfenster erzeugt, alle Eingabemasken werden zurueckgesetzt damit User Moeglichkeit hat ein
            /// weiteres Polygon zu erzeugen
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void BtnCreatePolygon_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
            {
                this.TxtBxValueHorizontal.Text = "";
                this.TxtBxValueVertical.Text = "";

                this.StPanelPolyPoints.Children.Clear();
                CustomInitialization();
            }

            /// <summary>
            /// Alle angeklickten Polygone werden farbig markiert
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void CanvasPaintground_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
            {
                Point mouseClickPoint = new Point{
                    X = e.GetPosition((Canvas)sender).X,
                    Y = e.GetPosition((Canvas)sender).Y
                };

                //Aufruf des Testes ob Polygon den geklickten Punkt umschliesst
                this._PointPolygonTester.StartTest(mouseClickPoint, this.CanvasPaintground.Children);
            }
        #endregion
    }
}
