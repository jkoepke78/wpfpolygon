﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPITest.Model
{
    public class PolygonGeometryData :
        INotifyPropertyChanged
    {
        #region variables
            private ObservableCollection<Point> _Points = new ObservableCollection<Point>();
            private double _OffSetX = 0;
            private double _OffSetY = 0;

            
            public event PropertyChangedEventHandler PropertyChanged;
        #endregion


        #region functions
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            private void OnPropertyChanged(string propertyName)
            {
                // take a copy to prevent thread issues
                PropertyChangedEventHandler handler = PropertyChanged;

                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        #endregion


        #region properties
            /// <summary>
            /// Liste aller Eckpunkte des neu zu erzeugenden Polygons
            /// </summary>
            public ObservableCollection<Point> Points
            {
                get { return this._Points; }
                set { this._Points = value; }
            }

            /// <summary>
            /// 
            /// </summary>
            public double OffsetX 
            { 
                get { return this._OffSetX; }
                set 
                { 
                    this._OffSetX = value;
                    this.OnPropertyChanged("OffsetX");
                } 
            }

            /// <summary>
            /// 
            /// </summary>
            public double OffsetY 
            {
                get { return this._OffSetY; }
                set 
                { 
                    this._OffSetY = value;
                    this.OnPropertyChanged("OffsetY");
                } 
            }
        #endregion
        
    }
}
