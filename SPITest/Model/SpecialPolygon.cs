﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SPITest.Model
{
    /// <summary>
    /// Polygon mit n Ecken
    /// </summary>
    public class SpecialPolygon 
    {
        /// <summary>
        /// Farbe des Polygons falls dieses selektiert ist
        /// </summary>
        public static Brush SelectedPolygonColor
        {
            get { return System.Windows.Media.Brushes.LightGreen; }
        }

        /// <summary>
        /// DefaultFarbe des Polygons
        /// </summary>
        public static Brush DefaultPolygonColor
        { 
            get { return System.Windows.Media.Brushes.LightGray;}
        }  
    }
}
